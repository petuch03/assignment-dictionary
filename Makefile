ASM=nasm
ASMFLAGS=-f elf64
OBJECT=-o
LD=ld

all: main

main: main.o dict.o lib.o
	$(LD) $(OBJECT) $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) $(OBJECT) $@ $<

main.o: main.asm lib.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) $(OBJECT) $@ $<

clean: 
	rm -f *.o main

.PHONY: clean