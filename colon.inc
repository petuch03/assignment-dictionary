%macro colon 2
%ifid %2
    %ifdef LAST_POINTER
        %2: dq LAST_POINTER
    %else
        %2: dq 0
    %endif
    %define LAST_POINTER %2
%else
    %fatal "invalid id"
%endif 

%ifstr %1
    %%key:   db   %1, 0
%else
    %error "ids and numbers cant be used as keys"
%endif
%endmacro