%include "colon.inc"
%include "words.inc"

global _start

extern print_string
extern read_word
extern exit
extern find_word
extern print_newline
extern string_length

%define STD_OUT 0x01
%define STD_ERR 0x02
%define BUFFER_SIZE 256
%define FAIL_EXIT_CODE 1

section .rodata
info_not_found: db "key not found", 10, 0
info_bad_request: db "bad request", 10, 0

section .text
_start:
    sub rsp, BUFFER_SIZE

    mov rdi, rsp
    mov rsi, BUFFER_SIZE

    call read_word
    test rax, rax      ; 0 if error occured else contains pointer
    je .print_bad_request

    mov rdi, rax
    mov rsi, LAST_POINTER ; LAST_POINTER is pointer to the last word in dict
    call find_word
    test rax, rax
    je .print_not_found

    mov rdi, rax 
    add rdi, 8      ; get address to key
    push rdi
    call string_length ; calculate key length to move pointer to value instead of key
    pop rdi
    add rdi, rax  
    inc rdi     ; goto value (key length + 1)
    mov rsi, STD_OUT
    call print_string
    call print_newline
    add rsp, BUFFER_SIZE
    xor rdi, rdi
    call exit

.print_bad_request:
    mov rdi, info_bad_request
    mov rsi, STD_ERR
    call print_string
    add rsp, BUFFER_SIZE
    mov rdi, FAIL_EXIT_CODE
    call exit

.print_not_found:
    mov rdi, info_not_found
    mov rsi, STD_ERR
    call print_string
    add rsp, BUFFER_SIZE
    mov rdi, FAIL_EXIT_CODE
    call exit
