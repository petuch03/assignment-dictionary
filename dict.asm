global find_word

extern string_equals

%define BYTE 8

section .text
find_word:
    test rsi, rsi ; pointer to last word
    je .not_found
    add rsi, BYTE
    push rdi    ; pointer to key(string)
    push rsi
    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1   ;test rax, rax 1 - equals, 0 - not equal
    jz  .get_element_ptr
    mov rsi, [rsi - BYTE]; back to pointer to next key value
    jmp find_word

.get_element_ptr:
    sub rsi, BYTE ; back to start of string 
    mov rax, rsi
    ret

.not_found:
    mov rax, 0 ; 0 for not found
    ret
